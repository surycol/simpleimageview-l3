﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using SimpleImageList.Models;
using Microsoft.Extensions.Logging;
using Microsoft.AspNetCore.Http;
using System.IO;
using Microsoft.AspNetCore.Hosting;

namespace SimpleImageList.Controllers
{
    [Route("api/[controller]")]
    public class ImagesController : Controller
    {
        static private List<Image> _images = new List<Image>();
        private ILogger<ImagesController> _logger;
        private IHostingEnvironment _environment;

        static public void popluteImagesData()
        {
            Image image = new Image
            {
                id = Guid.NewGuid(),
                title = "Sleeping Vi Vi",
                path = "/images/IMG_1416.JPG"
            };

            _images.Add(image);

            image = new Image
            {
                id = Guid.NewGuid(),
                title = "My big hand",
                path = "/images/IMG_1811.JPG"
            };
            _images.Add(image);

            image = new Image
            {
                id = Guid.NewGuid(),
                title = "Have a good dream",
                path = "/images/IMG_7740.JPG"
            };
            _images.Add(image);

            image = new Image
            {
                id = Guid.NewGuid(),
                title = "Big and small cat",
                path = "/images/IMG_8060.JPG"
            };
            _images.Add(image);
        }
       
        public ImagesController(ILogger<ImagesController> logger, IHostingEnvironment environment)
        {
            _logger = logger;
            _environment = environment;
        }

        // GET api/values
        [HttpGet]
        public IEnumerable<Image> Get()
        {
            _logger.LogDebug("Images set size:" + _images.Count());
            return _images;
        }

        // GET api/values/5
        [HttpGet("{id}")]
        public Image Get(String id)
        {
            Image img = _images.Where(t => t.id == new Guid(id)).FirstOrDefault();

            return img;
        }

        // POST api/values
        [HttpPost]
        public IEnumerable<Image> Post([FromBody]Image value)
        {
            _logger.LogDebug("Post:" + value);
            value.id = Guid.NewGuid();

            _images.Add(value);
            return _images;

        }

        // PUT api/values/5
        [HttpPut("{id}")]
        public IEnumerable<Image> Put(String id, [FromBody]Image value)
        {
            Image img = _images.Where(t => t.id == new Guid(id)).FirstOrDefault();
            img.title = value.title;
            img.path = value.path;

            return _images;
        }


        // DELETE api/values/5
        [HttpDelete("{id}")]
        public IEnumerable<Image> Delete(String id)
        {
            Image img = _images.Where(t => t.id == new Guid(id)).FirstOrDefault();
            _images.Remove(img);

            return _images;
        }

        //[HttpPost("Upload")]
        //public async Task<IEnumerable<String>> Upload(ICollection<IFormFile> files)
        //{
        //    var uploads = Path.Combine(_environment.WebRootPath, "images");
        //    List<string> imagePaths = new List<string>();

        //    foreach (var file in files)
        //    {
        //        if (file.Length > 0)
        //        {
        //            string path = "/images/" + file.FileName;

        //            using (var fileStream = new FileStream(Path.Combine(uploads, file.FileName), FileMode.Create))
        //            {
        //                await file.CopyToAsync(fileStream);
        //            }
        //            imagePaths.Add(path);
        //        }
        //    }
        //    return imagePaths;
        //}

        [HttpPost("Upload")]
        public async Task<IEnumerable<Image>> Upload(string title, ICollection<IFormFile> files)
        {
            var uploads = Path.Combine(_environment.WebRootPath, "images");
           
            foreach (var file in files)
            {
                if (file.Length > 0)
                {
                    string path = "/images/"+file.FileName;

                    using (var fileStream = new FileStream(Path.Combine(uploads, file.FileName), FileMode.Create))
                    {
                        await file.CopyToAsync(fileStream);
                    }

                    Image newImage = new Image();
                    newImage.path = path;
                    newImage.title = title;

                    _images.Add(newImage);
                }
            }
            return _images;
        }
    }
}
