﻿'use strict'

imageListApp.factory('imagesData', function ($resource) {
    return $resource('/api/images', {}, {
        get: { method: 'GET', isArray: true }
    });
});