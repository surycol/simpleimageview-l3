﻿'use strict'

imageListApp.controller('imageListController', 
    function start($scope, imagesData, Upload) {
        $scope.images = []
        $scope.selectImageIndex = 0
        $scope.listTitle = "Vivi Happy Album"

        function reloadImages(data) {
            $scope.images = data

            $scope.images.sort(function (item1, item2) {
                if (item1.title < item2.title)
                    return -1;
                if (item1.title > item2.title)
                    return 1;
                return 0;
            })

            var i = 0;
            $scope.images.forEach(function (element) {
                element.index = i++;
            });
        }

        var imagesQuery = imagesData.get()
        imagesQuery.$promise.then(function (data) {
            reloadImages(data)
        })
         
        // Watch carousel image change and set select image
        $scope.$watch(function () {
            var l = $scope.images.length
            for (var i = 0; i < l; i++) {
                if ($scope.images[i].active) {
                    return $scope.images[i];
                }
            }
        }, function (currentSlide, previousSlide) {
            if (currentSlide !== previousSlide) {
                console.log('current Image:', currentSlide);
                $scope.selectImageIndex = currentSlide.index
            }
            });

        $scope.listSelectImage = function (index) {
            $scope.selectImageIndex = index;

            // Find the image with select index and set to active.
            var l = $scope.images.length
            for (var i = 0; i < l; i++) {
                if ($scope.images[i].index === index) {
                    $scope.images[i].active = true
                }
            }
        }


        $scope.uploadFiles = function (file) {
            $scope.f = file;
           
            if (file) {
                file.upload = Upload.upload({
                    url: '/api/images/upload',
                    data: { title: $scope.picTitle, files: file }
                });

                file.upload.then(function (response) {
                   reloadImages(response.data);
                   
                }, function (response) {
                    if (response.status > 0)
                        $scope.errorMsg = response.status + ': ' + response.data;
                }, function (evt) {
                    file.progress = Math.min(100, parseInt(100.0 *
                        evt.loaded / evt.total));
                });
            }
        }

    }
)
 